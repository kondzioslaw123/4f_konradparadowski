<?php
function polaczZBD()
{
	try
	{
		$polaczenie = new mysqli('localhost', 'root', '', 'projekt');
		
		if($polaczenie->connect_errno)
		{
			throw new RuntimeException("Błąd połączenia z bazą.");
		}
		else
		{
			return $polaczenie;
		}
	}
	catch (RuntimeException $e)
	{
		echo $e->getMessage();
	}
}
?>